#ifndef _IMAGE_
#define _IMAGE_

#include "image.h"

#endif
#include <stdint.h>

enum read_status  {
  READ_OK = 0,
    READ_INVALID_FILE,
    READ_INVALID_HEADER,
    READ_INVALID_DATA,
    READ_INVALID_PADDING
};

enum  write_status  {
  WRITE_OK = 0,
  WRITE_INVALID_FILE,
  WRITE_HEADER_ERROR,
  WRITE_DATA_ERROR,
  WRITE_PADDING_ERROR,
  WRITE_ERROR
};

enum read_status from_bmp( FILE* file, struct image* img );
enum write_status to_bmp( FILE* file,const struct image *img );
