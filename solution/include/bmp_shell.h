#include "bmp.h"
#include "file_work.h"
#include <stdint.h>

void read_bmp( const char *file_name, struct image* img );
void write_bmp( const char *file_name, struct image* img );
