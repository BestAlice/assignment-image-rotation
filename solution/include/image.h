#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel  { uint8_t b, g, r; } __attribute__((packed));

struct image create_image(uint64_t height, uint64_t width);
void delete_image(struct image* img);
