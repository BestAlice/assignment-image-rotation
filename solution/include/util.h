#include <stdio.h>
#include <inttypes.h>

void print_str(const char line[]);
void print_err(const char* text, const uint8_t error_code);
void print_uint8(const uint8_t num);
void print_uint16(const uint16_t num);
void print_uint32(const uint32_t num);
void print_uint64(const uint64_t num);
