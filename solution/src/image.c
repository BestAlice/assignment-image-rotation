#include "image.h"
#include <malloc.h>
#include <stdint.h>

struct image create_image(uint64_t height, uint64_t width){
    return (struct image) {
            .height = height,
            .width = width,
            .data = malloc(sizeof(struct pixel) *  height * width)
    };
}

void free_image_data(struct image* img){
    free(img->data);
}

void delete_image(struct image* img){
    free_image_data(img);
}
