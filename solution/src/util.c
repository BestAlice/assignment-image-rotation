#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

void print_str(const char line[]){
    printf("%s\n", line);
}

void print_err(const char* text, const uint8_t error_code){
    fprintf(stderr, "%s: %"PRId8"\n", text, error_code);
    if (error_code != 0){
        exit(error_code);
    }
}

void print_uint8(const uint8_t num){
    printf("%"PRIu8"\n", num);
}

void print_uint16(const uint16_t num){
    printf("%"PRIu16"\n", num);
}

void print_uint32(const uint32_t num){
    printf("%"PRIu32"\n", num);
}

void print_uint64(const uint64_t num){
    printf("%"PRIu64"\n", num);
}

