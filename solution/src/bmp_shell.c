#include "bmp.h"
#include "file_work.h"
#include "util.h"

void read_bmp( const char *file_name, struct image* img ){
    FILE *image_file;

    print_err("OPEN ", open_file(file_name, &image_file));
    print_err("READ BMP", from_bmp(image_file, img));
    print_err("CLOSE ",close_file(&image_file));    
}

void write_bmp( const char *file_name, struct image* img ){
    FILE *image_file;

    print_err("CREATE ", create_file(file_name, &image_file));
    print_err("WRITE BMP", to_bmp(image_file, img));
    print_err("CLOSE ", close_file(&image_file));
}
