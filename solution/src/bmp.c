#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>

static const uint16_t BF_TYPE = 19778;
static const uint32_t BF_RESERVED = 0;
static const uint32_t BI_SIZE = 40;
static const uint16_t BI_PLANES = 0;
static const uint16_t BIT_COUNT = 24;
static const uint32_t BI_COMPRESSION = 0;
static const uint32_t BI_X_PELS_PER_METER = 0;
static const uint32_t BI_Y_PELS_PER_METER = 0;
static const uint32_t BI_CLR_USED = 0;
static const uint32_t BI_CLR_IMPORTANT = 0;

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static size_t find_padding(uint64_t width){
    return width % 4; 
}

struct bmp_header create_bmp_header(const struct image *img){
  struct bmp_header header = {0};
  uint32_t bi_size_image  = sizeof(struct pixel) * img->width * img->height +
                            find_padding(img->width) * img->height;
  uint32_t header_size = sizeof(struct bmp_header); 

  header.bfType            =  BF_TYPE; 
  header.bfileSize         =  header_size + bi_size_image;
  header.bfReserved        =  BF_RESERVED;
  header.bOffBits          =  header_size;
  header.biSize            =  BI_SIZE;
  header.biWidth           =  img->width; 
  header.biHeight          =  img->height;
  header.biPlanes          =  BI_PLANES;
  header.biBitCount        =  BIT_COUNT;
  header.biCompression     =  BI_COMPRESSION;
  header.biSizeImage       =  bi_size_image;
  header.biXPelsPerMeter   =  BI_X_PELS_PER_METER;
  header.biYPelsPerMeter   =  BI_Y_PELS_PER_METER;
  header.biClrUsed         =  BI_CLR_USED;
  header.biClrImportant    =  BI_CLR_IMPORTANT;

  return header;
}

enum read_status from_bmp( FILE* file, struct image* img ){
    struct bmp_header header = { 0 };

    if(!file) {
      return READ_INVALID_FILE;
      delete_image(img);
    }
    
    if(! fread( &header, sizeof( struct bmp_header ), 1, file )){
      return READ_INVALID_HEADER;
      delete_image(img);
    }

    *img = create_image((uint64_t)header.biHeight, (uint64_t)header.biWidth);
    size_t padding = find_padding(img->width);
    
    for (uint64_t i = 0; i < img->height; ++i) {
      if (! fread(&(img->data[i * (img->width)]), (size_t) img->width * sizeof(struct pixel), 1, file)){
        return READ_INVALID_DATA; 
      }
      if (fseek(file, padding, SEEK_CUR)) {
        return READ_INVALID_PADDING;
      }
    }
    return READ_OK;
}


enum write_status to_bmp( FILE* file,const struct image *img ) {
  if (!file) {
    return WRITE_INVALID_FILE;
  }
  struct bmp_header header = create_bmp_header(img);

  if (! fwrite(&header, sizeof(struct bmp_header), 1, file)){
    return WRITE_HEADER_ERROR;
  }

  size_t padding = find_padding(img->width);
  const uint64_t empty_data = 0;

  for (uint64_t i = 0; i<img->height; i++) {
    if (! fwrite(&(img->data[i*img->width]), sizeof(struct pixel)*img->width, 1, file)){
      return WRITE_DATA_ERROR;
    }
    if (!fwrite(&empty_data, padding, 1, file)) {
      return WRITE_PADDING_ERROR;
    }
  }
  return WRITE_OK;
}


