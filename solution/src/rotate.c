#include "rotate.h"
#include <stdio.h>


struct image rotate(const struct image img){
    struct image new_img = create_image(img.width, img.height );
    for (uint64_t row=0; row< new_img.height; row++){
        for (uint64_t col=0; col< new_img.width; col++){
            new_img.data[row * new_img.width + col] = img.data[(new_img.width - 1 - col) * new_img.height + row];
        }
    }
    return new_img;
}
