#include "bmp_shell.h"
#include "rotate.h"
#include "util.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>



int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc < 3) print_err("Not enough arguments \n", 1);
    if (argc > 3) print_err("Too many arguments \n", 2);

    struct image image;
    struct image rotate_image;

    char* in_file_name = argv[1]; 
    char* out_file_name = argv[2];

    print_str(in_file_name);
    read_bmp(in_file_name, &image);
    rotate_image = rotate(image);
    delete_image(&image);
    write_bmp(out_file_name, &rotate_image);
    delete_image(&rotate_image);
    return 0;
}
